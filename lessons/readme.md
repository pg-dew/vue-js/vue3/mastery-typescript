# Vue 3 + Typescript

###### Updated: 09/03/2564

## Table of content

- [Lesson 1: Why Vue & TypeScript](./1/readme.md)
- [Lesson 2: Setting Up Vue 3 & Typescript](./2/readme.md)
- [Lesson 3: Creating Components with TypeScript](./3/readme.md)
- [Lesson 4: Type Fundamentals](./4/readme.md)
- [Lesson 5: Defining Custom Types](./5/readme.md)
- [Lesson 6: Data with Custom Types](./6/readme.md)
- [Lesson 7: Props with Types](./7/readme.md)
- [Lesson 8: Computed & Methods with Custom Types](./8/readme.md)
- [Lesson 9: Next Steps](./9/readme.md)
- [Bonus: Composition API with TypeScript](./bonus/readme.md)
